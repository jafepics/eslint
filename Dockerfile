ARG SCANNER_VERSION=7.32.0

FROM golang:1.17-alpine AS gobuild

ENV CGO_ENABLED=0 GOOS=linux

WORKDIR /go/src/buildapp
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    PATH_TO_MODULE=`go list -m` && \
    go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer

FROM node:16.14-alpine3.15 as builder
RUN apk update && apk add apk-tools=2.12.7-r3

ARG SCANNER_VERSION

COPY package.json /
RUN yarn && yarn upgrade "eslint@$SCANNER_VERSION"

FROM node:16.14-alpine3.15

ARG SCANNER_VERSION
ENV SCANNER_VERSION $SCANNER_VERSION

# The node user below doesn't have permission to create a file in /etc/ssl/certs
# or /etc/gitconfig, this RUN command creates files that the analyzer can write to.
RUN mkdir -p /etc/ssl/certs/ && \
    touch /etc/ssl/certs/ca-certificates.crt && \
    chmod g+w /etc/ssl/certs/ca-certificates.crt

WORKDIR /home/node

COPY eslintrc ./.eslintrc
COPY babel.config.json .
COPY --from=builder  yarn.lock package.json ./
COPY --from=gobuild  /analyzer /analyzer

RUN yarn --frozen-lockfile && yarn cache clean

ENTRYPOINT []
CMD ["/analyzer", "run"]
