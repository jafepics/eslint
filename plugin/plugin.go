package plugin

import (
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

// Match checks and makes sure the filename has either a .js, .jsx, or .html extension
func Match(path string, info os.FileInfo) (bool, error) {
	switch filepath.Ext(info.Name()) {
	case ".js", ".jsx", ".html", ".ts", ".tsx":
		return true, nil
	default:
		return false, nil
	}
}

func init() {
	plugin.Register("eslint", Match)
}
